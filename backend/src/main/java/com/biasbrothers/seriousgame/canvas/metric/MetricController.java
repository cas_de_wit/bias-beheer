package com.biasbrothers.seriousgame.canvas.metric;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/metric")
public class MetricController {
    private final MetricService metricService;

    @Autowired
    public MetricController(MetricService metricService) {
        this.metricService = metricService;
    }

    @CrossOrigin
    @GetMapping
    public List<Metric> getAll() {
        return metricService.getAll();
    }

    @CrossOrigin
    @GetMapping(path = "/")
    public Metric getById(@RequestParam Long id) {
    return metricService.getById(id);
    }

    @CrossOrigin
    @PostMapping(path = "/add")
    public ResponseEntity<Metric> add(@RequestBody Metric metric) {
        Metric metricTest = metricService.add(metric);

        return new ResponseEntity<>(metricTest, HttpStatus.CREATED);
    }

    @CrossOrigin
    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<Object> delete(@PathVariable("id") Long id) {

        metricService.delete(id);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @CrossOrigin
    @PutMapping(path = "/update/")
    public ResponseEntity<Metric> update(@RequestBody Metric metric) {
        Metric metricTest = metricService.update(metric);

        return new ResponseEntity<>(metricTest, HttpStatus.ACCEPTED);
    }
}
