package com.biasbrothers.seriousgame.canvas;

import com.biasbrothers.seriousgame.canvas.map.MapRepository;
import com.biasbrothers.seriousgame.canvas.metric.MetricRepository;
import com.biasbrothers.seriousgame.newsarticle.NewsArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CanvasService {
    private final CanvasRepository canvasRepository;
    private final MapRepository mapRepository;
    private final MetricRepository metricRepository;
    private final NewsArticleRepository newsArticleRepository;

    @Autowired
    public CanvasService(CanvasRepository canvasRepository, MapRepository mapRepository, MetricRepository metricRepository, NewsArticleRepository newsArticleRepository) {
        this.canvasRepository = canvasRepository;
        this.mapRepository = mapRepository;
        this.metricRepository = metricRepository;
        this.newsArticleRepository = newsArticleRepository;
    }

    public List<Canvas> getAll() {
        return canvasRepository.findAll();
    }

    public Canvas getById(Long id) {
        return canvasRepository.findById(id).orElseThrow(() -> new IllegalStateException("canvas with id " + id + " does not exist"));
    }

    public Canvas add(Canvas canvas) {
        Canvas newCanvas = new Canvas();

        return saveCanvas(canvas, newCanvas);
    }

    public String delete(Long id) {
        canvasRepository.deleteById(id);
        return "SUCCESS";
    }

    public Canvas update(Canvas canvas) {
        Canvas newCanvas = canvasRepository.findById(
                canvas.getId()).orElseThrow(() -> new IllegalStateException("canvas with id " + canvas.getId() + " does not exist"));

        return saveCanvas(canvas, newCanvas);
    }

    private Canvas saveCanvas(Canvas canvas, Canvas newCanvas) {
        newCanvas.setPoints(canvas.getPoints());
        newCanvas.setValue1(canvas.getValue1());
        newCanvas.setValue2(canvas.getValue2());
        newCanvas.setValue3(canvas.getValue3());
        newCanvas.setName(canvas.getName());

        newCanvas.setMap(mapRepository.save(canvas.getMap().clone()));
        newCanvas.setMetrics(metricRepository.saveAll(canvas.getMetrics()));
        newCanvas.setNewsArticles(newsArticleRepository.saveAll(canvas.getNewsArticles()));

        return canvasRepository.save(newCanvas);
    }

}
