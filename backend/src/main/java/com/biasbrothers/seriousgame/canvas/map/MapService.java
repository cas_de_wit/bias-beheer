package com.biasbrothers.seriousgame.canvas.map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class MapService {
    private final MapRepository mapRepository;

    @Autowired
    public MapService(MapRepository mapRepository) {
        this.mapRepository = mapRepository;
    }

    public List<Map> getAll() {
        return mapRepository.findAll();
    }

    public Map getById(Long id) {
        return mapRepository.findById(id).orElseThrow(() ->
                new IllegalStateException("map with id " + id + " does not exist"));
    }

    public Map add(Map map) {
        Map newMap = new Map();

        return saveMap(map, newMap);
    }

    public String delete(Long id) {
        mapRepository.deleteById(id);
        return "SUCCESS";
    }

    @Transactional
    public Map update(Map map) {
        Map newMap = mapRepository.findById(map.getId()).orElseThrow(() ->
                new IllegalStateException("map with id " + map.getId() + " does not exist"));

        return  saveMap(map, newMap);
    }

    private Map saveMap(Map map, Map newMap) {
        newMap.setName(map.getName());
        newMap.setUrl(map.getUrl());

        return mapRepository.save(newMap);
    }
}
