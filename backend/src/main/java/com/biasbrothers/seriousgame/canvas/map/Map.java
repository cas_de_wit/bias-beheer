package com.biasbrothers.seriousgame.canvas.map;

import javax.persistence.*;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity(name = "Map")
@Table(
        name = "map",
        uniqueConstraints = {
                @UniqueConstraint(name = "map_url_unique", columnNames = "url")
        }
)
public class Map {

    @Id
    @SequenceGenerator(
            name = "map_sequence",
            sequenceName = "map_sequence",
            initialValue = 25,
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "map_sequence"
    )
    @Column(
            name = "id",
            updatable = false
    )
    private Long id;

    @Column(
            name = "name",
            nullable = false
    )
    private String name;

    @Column(
            name = "url",
            nullable = false,
            columnDefinition = "TEXT"
    )
    private String url;

    public Map() {

    }

    public Map(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public Map(Long id, String name, String url) {
        this.id = id;
        this.name = name;
        this.url = url;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public Map clone() {
        try {
            return (Map) super.clone();
        } catch (CloneNotSupportedException e) {
            return new Map(this.getId(), this.getName(), this.getUrl());
        }
    }
}
