package com.biasbrothers.seriousgame.canvas;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/canvas")
public class CanvasController {
    private final CanvasService canvasService;

    @Autowired
    public CanvasController(CanvasService canvasService) {
        this.canvasService = canvasService;
    }

    @CrossOrigin
    @GetMapping
    public List<Canvas> getAll() {
        return canvasService.getAll();
    }

    @CrossOrigin
    @GetMapping(path = "/")
    public Canvas getById(@RequestParam Long id) {
        return canvasService.getById(id);
    }

    @CrossOrigin
    @PostMapping(path = "/add")
    public ResponseEntity<Canvas> add(@RequestBody Canvas canvas) {
       Canvas canvasTest = canvasService.add(canvas);

        return new ResponseEntity<>(canvasTest, HttpStatus.CREATED);
    }

    @CrossOrigin
    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<Object> delete(@PathVariable("id") Long id) {
        canvasService.delete(id);

        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @CrossOrigin
    @PutMapping(path = "/update/")
    public ResponseEntity<Canvas> update(@RequestBody Canvas canvas) {
        Canvas canvasTest = canvasService.update(canvas);

        return new ResponseEntity<>(canvasTest, HttpStatus.ACCEPTED);
    }
}
