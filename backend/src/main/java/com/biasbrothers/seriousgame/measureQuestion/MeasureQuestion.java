package com.biasbrothers.seriousgame.measureQuestion;

import javax.persistence.*;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity(name = "MeasureQuestion")
@Table(
        name = "measure_question",
        uniqueConstraints = {
                @UniqueConstraint(name = "measure_question_unique", columnNames = "answer")
        }
)
public class MeasureQuestion {

    @Id
    @SequenceGenerator(
            name = "measure_question_sequence",
            sequenceName = "measure_question_sequence",
            initialValue = 19,
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "measure_question_sequence"
    )
    @Column(
            name = "id",
            updatable = false
    )
    private Long id;

    @Column(
            name = "points",
            nullable = false
    )
    private int points;

    @Column(
            name = "answer",
            nullable = false
    )
    private String answer;

    public MeasureQuestion() {

    }

    public MeasureQuestion(int points, String answer) {
        this.points = points;
        this.answer = answer;
    }

    public MeasureQuestion(Long id, int points, String answer) {
        this.id = id;
        this.points = points;
        this.answer = answer;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    @Override
    public MeasureQuestion clone() {
        try {
            return (MeasureQuestion) super.clone();
        } catch (CloneNotSupportedException e) {
            return new MeasureQuestion(this.getId(), this.getPoints(), this.getAnswer());
        }
    }
}
