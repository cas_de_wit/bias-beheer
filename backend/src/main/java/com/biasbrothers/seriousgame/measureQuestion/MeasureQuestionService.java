package com.biasbrothers.seriousgame.measureQuestion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class MeasureQuestionService {
    private final MeasureQuestionRepository measureQuestionRepository;

    @Autowired
    public MeasureQuestionService(MeasureQuestionRepository measureQuestionRepository) {
        this.measureQuestionRepository = measureQuestionRepository;
    }

    public List<MeasureQuestion> getAll() {
        return measureQuestionRepository.findAll();
    }

    public MeasureQuestion getById(Long id) {
        return measureQuestionRepository.findById(id).orElseThrow(() -> new IllegalStateException(
                "measure question with id " + id + " does not exist"));
    }

    public MeasureQuestion add(MeasureQuestion measureQuestion) {
        MeasureQuestion newMeasureQuestion = measureQuestion.clone();
        return measureQuestionRepository.save(newMeasureQuestion);
    }

    public String delete(Long id) {
        measureQuestionRepository.deleteById(id);
        return "SUCCESS";
    }

    @Transactional
    public MeasureQuestion update(MeasureQuestion measureQuestion) {
        MeasureQuestion newMeasureQuestion = measureQuestionRepository.findById(measureQuestion.getId()).orElseThrow(() -> new IllegalStateException(
                "measure question with id " + measureQuestion.getId() + " does not exist"));

        newMeasureQuestion.setAnswer(measureQuestion.getAnswer());
        newMeasureQuestion.setPoints(measureQuestion.getPoints());

        return measureQuestionRepository.save(newMeasureQuestion);
    }
}
