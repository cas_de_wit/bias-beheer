package com.biasbrothers.seriousgame.bias;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class BiasService {
    private final BiasRepository biasRepository;

    @Autowired
    public BiasService(BiasRepository biasRepository) {
        this.biasRepository = biasRepository;
    }

    public List<Bias> getAll() {
        return biasRepository.findAll();
    }

    public Bias getById(Long id) {
        return biasRepository.findById(id).orElseThrow(() ->
                new IllegalStateException("bias with id " + id + " does not exist"));
    }

    public Bias add(Bias bias) {
        Bias newBias = bias.clone();
        return biasRepository.save(newBias);
    }

    public String delete(Long id) {
        biasRepository.deleteById(id);
        return "SUCCESS";
    }

    @Transactional
    public Bias update(Bias bias) {
        Bias newBias = biasRepository.findById(bias.getId()).orElseThrow(() ->
        new IllegalStateException("bias with id " + bias.getId() + " does not exist"));

        newBias.setName(bias.getName());
        newBias.setDescription(bias.getDescription());
        newBias.setExample(bias.getExample());

        return biasRepository.save(newBias);
    }
}
