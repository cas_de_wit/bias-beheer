package com.biasbrothers.seriousgame.scenario;

import com.biasbrothers.seriousgame.measureQuestion.MeasureQuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ScenarioService {
    private final ScenarioRepository scenarioRepository;
    private final MeasureQuestionRepository measureQuestionRepository;

    @Autowired
    public ScenarioService(ScenarioRepository scenarioRepository, MeasureQuestionRepository measureQuestionRepository) {
        this.scenarioRepository = scenarioRepository;
        this.measureQuestionRepository = measureQuestionRepository;
    }

    public List<Scenario> getAll() {
        return scenarioRepository.findAll();
    }

    public Scenario getById(Long id) {
        return scenarioRepository.findById(id).orElseThrow(() -> new IllegalStateException(
                "scenario with id " + id + " does not exist"));
    }

    public Scenario add(Scenario scenario) {
        Scenario newScenario = new Scenario();
        return saveScenario(scenario, newScenario);
    }

    public String delete(Long id) {
        scenarioRepository.deleteById(id);
        return "SUCCESS";
    }

    @Transactional
    public Scenario update(Scenario scenario) {
        Scenario newScenario = scenarioRepository.findById(scenario.getId()).orElseThrow(() -> new IllegalStateException(
                "scenario with id " + scenario.getId() + " does not exist"));

        return saveScenario(scenario, newScenario);
    }

    private Scenario saveScenario(Scenario scenario, Scenario newScenario) {
        newScenario.setText(scenario.getText());
        newScenario.setTitle(scenario.getTitle());
        newScenario.setMeasureQuestions(measureQuestionRepository.saveAll(scenario.getMeasureQuestions()));

        return scenarioRepository.save(newScenario);
    }
}