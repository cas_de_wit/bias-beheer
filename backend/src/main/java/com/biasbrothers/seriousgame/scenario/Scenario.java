package com.biasbrothers.seriousgame.scenario;

import com.biasbrothers.seriousgame.measureQuestion.MeasureQuestion;

import javax.persistence.*;

import java.util.List;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity(name = "Scenario")
@Table(
        name = "scenario",
        uniqueConstraints = {
                @UniqueConstraint(name = "scenario_title_unique", columnNames = "title")
        }
)
public class Scenario {

    @Id
    @SequenceGenerator(
            name = "scenario_sequence",
            sequenceName = "scenario_sequence",
            initialValue = 7,
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "scenario_sequence"
    )
    @Column(
            name = "id",
            updatable = false
    )
    private Long id;

    @Column(
            name = "title",
            nullable = false
    )
    private String title;

    @Column(
            name = "text",
            nullable = false,
            columnDefinition = "TEXT"
    )
    private String text;

    @ManyToMany(cascade ={CascadeType.DETACH,
            CascadeType.MERGE,
            CascadeType.PERSIST})
    @JoinTable(name = "measure_question_scenario",
    joinColumns = @JoinColumn(name = "measure_question_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "scenario_id", referencedColumnName = "id"))
    private List<MeasureQuestion> measureQuestions;

    public Scenario() {

    }

    public Scenario(String title, String text, List<MeasureQuestion> measureQuestions) {
        this.title = title;
        this.text = text;
        this.measureQuestions = measureQuestions;
    }

    public Scenario(Long id, String title, String text, List<MeasureQuestion> measureQuestions) {
        this.id = id;
        this.title = title;
        this.text = text;
        this.measureQuestions = measureQuestions;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<MeasureQuestion> getMeasureQuestions() {
        return measureQuestions;
    }

    public void setMeasureQuestions(List<MeasureQuestion> measureQuestions) {
        this.measureQuestions = measureQuestions;
    }

    public void add(MeasureQuestion measureQuestion) {
        measureQuestions.add(measureQuestion);
    }

    public void remove(MeasureQuestion measureQuestion) {
        measureQuestions.remove(measureQuestion);
    }

    @Override
    public Scenario clone() {
        try {
            return (Scenario) super.clone();
        } catch (CloneNotSupportedException e) {
            return new Scenario(this.getId(), this.getTitle(), this.getText(), this.getMeasureQuestions());

        }
    }

    @Override
    public String toString() {
        return "Scenario{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", text='" + text + '\'' +
                ", measureQuestions=" + measureQuestions +
                '}';
    }
}
