package com.biasbrothers.seriousgame.biasQuestion;

import com.biasbrothers.seriousgame.bias.BiasRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class BiasQuestionService {
    private final BiasQuestionRepository biasQuestionRepository;
    private final BiasRepository biasRepository;

    @Autowired
    public BiasQuestionService(BiasQuestionRepository biasQuestionRepository, BiasRepository biasRepository) {
        this.biasQuestionRepository = biasQuestionRepository;
        this.biasRepository = biasRepository;
    }

    @Autowired
    public List<BiasQuestion> getAll() {
        return biasQuestionRepository.findAll();
    }

    public BiasQuestion getById(Long id) {
        return biasQuestionRepository.findById(id).orElseThrow(() ->
                new IllegalStateException("bias question with id " + id + " does not exist"));
    }

    public BiasQuestion add(BiasQuestion biasQuestion) {
        BiasQuestion newBiasQuestion = new BiasQuestion();

        return saveBiasQuestion(biasQuestion, newBiasQuestion);
    }

    public String delete(Long id) {
        biasQuestionRepository.deleteById(id);
        return "SUCCESS";
    }

    @Transactional
    public BiasQuestion update(BiasQuestion biasQuestion) {
        BiasQuestion newBiasQuestion = biasQuestionRepository.findById(biasQuestion.getId()).orElseThrow(() ->
                new IllegalStateException("bias question with id " + biasQuestion.getId() + " does not exist"));

        return saveBiasQuestion(biasQuestion, newBiasQuestion);
    }

    private BiasQuestion saveBiasQuestion(BiasQuestion biasQuestion, BiasQuestion newBiasQuestion) {
        newBiasQuestion.setPoints(biasQuestion.getPoints());
        newBiasQuestion.setBias(biasRepository.save(biasQuestion.getBias().clone()));

        return biasQuestionRepository.save(newBiasQuestion);
    }
}
