package com.biasbrothers.seriousgame.biasQuestion;

import com.biasbrothers.seriousgame.bias.Bias;
import com.biasbrothers.seriousgame.bias.BiasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/bias_question")
public class BiasQuestionController {
    private final BiasQuestionService biasQuestionService;

    @Autowired
    public BiasQuestionController(BiasQuestionService biasQuestionService) {
        this.biasQuestionService = biasQuestionService;
    }

    @CrossOrigin
    @GetMapping
    public List<BiasQuestion> getAll() {
        return biasQuestionService.getAll();
    }

    @CrossOrigin
    @GetMapping(path = "/")
    public BiasQuestion getById(@RequestParam Long id) {
        return biasQuestionService.getById(id);
    }

    @CrossOrigin
    @PostMapping(path = "/add")
    public ResponseEntity<BiasQuestion> add(@RequestBody BiasQuestion biasQuestion) {
        BiasQuestion biasQuestionTest = biasQuestionService.add(biasQuestion);

        return new ResponseEntity<>(biasQuestionTest, HttpStatus.CREATED);
    }

    @CrossOrigin
    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<Object> delete(@PathVariable("id") Long id) {
        biasQuestionService.delete(id);

        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @CrossOrigin
    @PutMapping(path = "/update/")
    public ResponseEntity<BiasQuestion> update(@RequestBody BiasQuestion biasQuestion) {
       BiasQuestion biasQuestionTest = biasQuestionService.update(biasQuestion);

        return new ResponseEntity<>(biasQuestionTest, HttpStatus.ACCEPTED);
    }
}