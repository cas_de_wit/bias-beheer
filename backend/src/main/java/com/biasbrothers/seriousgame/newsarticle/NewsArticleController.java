package com.biasbrothers.seriousgame.newsarticle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/news_article")
public class NewsArticleController {
    private final NewsArticleService newsArticleService;

    @Autowired
    public NewsArticleController(NewsArticleService newsArticleService) {
        this.newsArticleService = newsArticleService;
    }

    @CrossOrigin
    @GetMapping
    public List<NewsArticle> getAll() {
        return newsArticleService.getAll();
    }

    @CrossOrigin
    @GetMapping(path = "/")
    public NewsArticle getById(@RequestParam Long id) {
        return newsArticleService.getById(id);
    }

    @CrossOrigin
    @PostMapping(path = "/add")
    public ResponseEntity<NewsArticle> add(@RequestBody NewsArticle newsArticle) {
        NewsArticle newsArticleTest = newsArticleService.add(newsArticle);

        return new ResponseEntity<>(newsArticleTest, HttpStatus.CREATED);
    }

    @CrossOrigin
    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<Object> delete(@PathVariable("id") Long id) {
        newsArticleService.delete(id);

        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @CrossOrigin
    @PutMapping(path = "/update/")
    public ResponseEntity<NewsArticle> update(@RequestBody NewsArticle newsArticle) {
        NewsArticle newsArticleTest = newsArticleService.update(newsArticle);

        return new ResponseEntity<>(newsArticleTest, HttpStatus.ACCEPTED);
    }
}