import './styles/styleStandard.css';
import './styles/styleButtons.css';
import './styles/styleContent.css';
import Navbar from './components/navBar/Navbar';
import GameMenu from './components/game/GameMenu';
import BiasMenu from './components/bias/BiasMenu';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import { useEffect, useState } from 'react';
import ScenarioMenu from './components/scenario/ScenarioMenu';
import MapMenu from './components/map/MapMenu';
import NewsArticleMenu from './components/newsarticle/NewsArticleMenu';
import MetricsMenu from "./components/metrics/MetricsMenu";
import MeasureMenu from "./components/measure/MeasureMenu";
import CanvasMenu from './components/canvas/CanvasMenu';
import TimerMenu from './components/timer/TimerMenu';
import Home from './components/home/Home';
import Login from './components/Login/Login';
import React from 'react';
import axios from 'axios';
import RoundMenu from './components/round/RoundMenu'

function App() {
    const [pageIndex, setPageIndex] = useState(0);
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [user, setUser] = useState();
    const temporary = true;

    useEffect(() => {
        const loggedInUser = sessionStorage.getItem("user");
        if (loggedInUser) {
            const foundUser = JSON.parse(loggedInUser);
            setUser(foundUser);
        }
    }, []);

    // logout the user
    const handleLogout = () => {
        setUser(null);
        setUsername("");
        setPassword("");
        sessionStorage.clear();
    };

    // login the user
    const handleSubmit = async e => {
        e.preventDefault();
        const user = { username, password };
        // send the username and password to the server
        const response = await axios.post(
            "http://blogservice.herokuapp.com/api/login",
            user
        );
        // set the state of the user
        setUser(response.data);
        // store the user in sessionStorage
        sessionStorage.setItem("user", JSON.stringify(response.data));
    };

    return (
        <Router>
            <div className="container-app">
                {temporary ? <Navbar setPageIndex={setPageIndex} pageIndex={pageIndex} loggingOut={handleLogout} /> : ''}
                {temporary ?
                    <Switch>
                        <Route exact path="/" component={Home} />
                        {/* <Route exact path="/game" component={GameMenu} /> */}
                        <Route exact path="/round" component={RoundMenu} />
                        <Route exact path="/canvas" component={CanvasMenu} />
                        <Route exact path="/nieuwsberichten" component={NewsArticleMenu} />
                        <Route exact path="/bias" component={BiasMenu} />
                        <Route exact path="/scenario" component={ScenarioMenu} />
                        <Route exact path="/maatregelen" component={MeasureMenu} />
                        <Route exact path="/kaarten" component={MapMenu} />
                        <Route exact path="/metrieken" component={MetricsMenu} />
                        <Route exact path="/timer" component={TimerMenu} />
                    </Switch>
                    : <Login handleSubmit={handleSubmit} setUsername={setUsername} username={username} setPassword={setPassword} password={password} />}
            </div>
        </Router>
    );


}

export default App;


