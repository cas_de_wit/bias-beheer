import SearchIcon from '@mui/icons-material/Search';

const SearchBar = ({ searchForItems }) => {

    const handleEnter = (e) => {
        e.preventDefault();
    }

    return (
        <section className="search">
            <form onSubmit={handleEnter}>
                <div className="searchInputs">
                    <input type="text" placeholder="Zoek een menu item" onChange={searchForItems} />
                    <div className="searchIcon">
                        <SearchIcon />
                    </div>
                </div>
            </form>
        </section>
    );
}

export default SearchBar;
