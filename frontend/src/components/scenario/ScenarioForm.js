import { useState, useEffect, useCallback } from "react";

const ScenarioForm = ({ postOrPut,
    title, setTitle,
    text, setText,
    measureQuestion1, measureQuestion2, measureQuestion3,
    setMeasureQuestion1, setMeasureQuestion2, setMeasureQuestion3,
    openModalHandler, greyedOut
}) => {
    const [measureList, setMeasureList] = useState([]);

    const fetchMeasures = useCallback(async () => {
        const res = await fetch('http://localhost:8080/api/measure_question');
        const data = await res.json();
        setMeasureList(data);
    }, []);

    useEffect(() => {
        const fetchData = async () => {
            fetchMeasures();
        }
        fetchData();
    }, [fetchMeasures]);

    const handleDropdownSelect = (e, index) => {
        measureList.forEach(measure => {
            if (index === 1) {
                if (e.target.value === measure.answer) {
                    setMeasureQuestion1({ id: measure.id, answer: e.target.value, points: measure.points });
                }
            } else if (index === 2) {
                if (e.target.value === measure.answer) {
                    setMeasureQuestion2({ id: measure.id, answer: e.target.value, points: measure.points });
                }
            } else if (index === 3) {
                if (e.target.value === measure.answer) {
                    setMeasureQuestion3({ id: measure.id, answer: e.target.value, points: measure.points });
                }
            }
        });
    }

    return (
        <>
            <form className="scenarioContent-form" onSubmit={postOrPut}>
                <label id="scenarioTitel">Scenario titel</label>
                <textarea value={title} onChange={e => setTitle(e.target.value)} />
                <label>Scenario:</label>
                <textarea value={text} onChange={e => setText(e.target.value)} />
                <div className="textareas-answers">
                    <div className="textarea-rows">
                        <label>Maatregel 1</label>
                        <select onChange={e => handleDropdownSelect(e, 1)}>
                            <option defaultValue="Kies een andere maatregel">Kies een andere maatregel</option>
                            {measureList.map((measure) => <option key={measure.id} value={measure.answer}>{measure.answer}</option>)}
                        </select>
                        <label>Punten</label>
                    </div>
                    <div className="textarea-rows textarea-rows2">
                        <textarea className={greyedOut ? "greyed-out" : undefined} value={measureQuestion1.answer} onChange={e => setMeasureQuestion1({ ...measureQuestion1, answer: e.target.value })} />
                        <textarea className={greyedOut ? "greyed-out" : undefined} value={measureQuestion1.points} onChange={e => setMeasureQuestion1({ ...measureQuestion1, points: e.target.value })} />
                    </div>
                    <div className="textarea-rows">
                        <label>Maatregel 2</label>
                        <select onChange={e => handleDropdownSelect(e, 2)}>
                            <option defaultValue="Kies een andere maatregel">Kies een andere maatregel</option>
                            {measureList.map((measure) => <option key={measure.id} value={measure.answer}>{measure.answer}</option>)}
                        </select>
                        <label>Punten</label>
                    </div>
                    <div className="textarea-rows textarea-rows2">
                        <textarea className={greyedOut ? "greyed-out" : undefined} value={measureQuestion2.answer} onChange={e => setMeasureQuestion2({ ...measureQuestion2, answer: e.target.value })} />
                        <textarea className={greyedOut ? "greyed-out" : undefined} value={measureQuestion2.points} onChange={e => setMeasureQuestion2({ ...measureQuestion2, points: e.target.value })} />
                    </div>
                    <div className="textarea-rows">
                        <label>Maatregel 3</label>
                        <select onChange={e => handleDropdownSelect(e, 3)}>
                            <option defaultValue="Kies een andere maatregel">Kies een andere maatregel</option>
                            {measureList.map((measure) => <option key={measure.id} value={measure.answer}>{measure.answer}</option>)}
                        </select>
                        <label>Punten</label>
                    </div>
                    <div className="textarea-rows textarea-rows2">
                        <textarea className={greyedOut ? "greyed-out" : undefined} value={measureQuestion3.answer} onChange={e => setMeasureQuestion3({ ...measureQuestion3, answer: e.target.value })} />
                        <textarea className={greyedOut ? "greyed-out" : undefined} value={measureQuestion3.points} onChange={e => setMeasureQuestion3({ ...measureQuestion3, points: e.target.value })} />
                    </div>
                </div>
                <div className="div-save-button">
                    <button className="button-delete" onClick={openModalHandler}>Verwijder Scenario</button>
                    <input className="button-save" type="submit" value="Opslaan" />
                </div>
            </form>
        </>
    );
}

export default ScenarioForm;
