import { useCallback, useEffect, useState } from "react";
import BiasAdd from "./BiasAdd";
import ButtonList from "../ButtonList";
import BiasContent from "./BiasContent";
import ModalMenu from "../modal/ModalMenu";
import Backdrop from "../modal/Backdrop"
import SearchIcon from '@mui/icons-material/Search';

const BiasMenu = () => {
    const [biasList, setBiasList] = useState([]);
    const [biasContent, setBiasContent] = useState([]);
    const [chosenIndex, setChosenIndex] = useState(0);
    const [newBias, addBias] = useState(false);
    const [listForButtons, setListForButtons] = useState([]);
    const [openModalMenu, setOpenModalMenu] = useState(false);
    const [searchInput, setSearchInput] = useState('');

    const fetchAllBiases = useCallback(async () => {
        const res = await fetch('http://localhost:8080/api/bias');
        const data = await res.json();
        setBiasList(data);
        setListForButtons(data);
        setChosenIndex(false);
        addBias(false);
    }, []);

    useEffect(() => {
        const fetchData = async () => {
            fetchAllBiases();
        }
        fetchData();
    }, [fetchAllBiases]);

    const handleBiasClick = (clickedBiasId) => {
        biasList.forEach(bias => {
            if (bias.id === clickedBiasId) {
                addBias(false);
                setBiasContent(bias);
                setChosenIndex(clickedBiasId);
                setOpenModalMenu(false);
                setSearchInput('');
            }
        });
    }

    const clickAddBias = () => {
        setChosenIndex(false);
        addBias(true);
    }

    const cancelHandler = () => {
        setOpenModalMenu(false);
        setSearchInput('');
    }

    const openMenu = () => {
        setOpenModalMenu(true);
    }

    // Set the list for the modalmenu
    const returnFilteredData = (searchValue) => {
        if (searchValue !== '') {
            return biasList.filter((item) => {
                return Object.values(item).join('').toLowerCase().includes(searchInput.toLowerCase())
            });
        } else {
            return biasList;
        }
    }

    return (
        <div className="container-menu">
            <div className="btn-group">
                {biasList.length > 0 && <ButtonList list={listForButtons} handleItemClick={handleBiasClick} color={chosenIndex} />}
                <button onClick={clickAddBias} title="addBias">+</button>
            </div>
            <div className="container-content">
                {!newBias && !chosenIndex ? 'Selecteer een bias of maak een nieuwe aan.' : ''}
                {chosenIndex && <BiasContent biasContent={biasContent} fetchAllBiases={fetchAllBiases} />}
                {newBias ? <BiasAdd fetchAllBiases={fetchAllBiases} /> : ''}
            </div>
            <button className="button-menu" onClick={openMenu} title="menu"><SearchIcon /></button>
            <div>
                <button className="button-add" onClick={clickAddBias}>+</button>
            </div>
            <div>
                {openModalMenu && <ModalMenu modalQuestion="Maak een keuze uit het menu" cancelHandler={cancelHandler} modalCancelText="Annuleren" handleClick={handleBiasClick} listForButtons={returnFilteredData(searchInput)} listForResults={(e) => setSearchInput(e.target.value)} />}
                {openModalMenu && <Backdrop cancelModal={cancelHandler} />}
            </div>
        </div>
    );
}

export default BiasMenu;
