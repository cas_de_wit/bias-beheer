import '@testing-library/jest-dom/extend-expect';
import { render, screen, fireEvent } from '@testing-library/react';
import BiasMenu from './BiasMenu';


// Test of het menu wordt ingeladen.
// test('On render, the buttons are loaded', () => {
//     render(<BiasMenu />);

//     screen.debug();
// })

// Kijkt of de addBias knop goed wordt ingeladen
it('Check if addBias renders correct', () => {
    const { queryByTitle } = render(<BiasMenu />);
    const btn = queryByTitle("addBias");
    expect(btn).toBeTruthy();
})

// Checkt of de add button het juiste component inlaad
describe('Check the add button', () => {
    it('onClick', () => {
        const { queryByTitle } = render(<BiasMenu />);
        const btn = queryByTitle("addBias");
        fireEvent.click(btn);
        expect(document.getElementById("biasNaam")).toBeInTheDocument();
    });
})

// Checkt of de menu modal wordt geopend
describe('Check the menu button', () => {
    it('onClick', () => {
        const { queryByTitle } = render(<BiasMenu />);
        const btn = queryByTitle("menu");
        fireEvent.click(btn);
        expect(document.getElementById("menuId")).toBeInTheDocument();
    });
})

