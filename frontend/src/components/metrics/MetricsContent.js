import { useState, useEffect } from "react";
import Backdrop from "../modal/Backdrop";
import checkModal from "../../Functions/checkModal";

const MetricsContent = ({ metricsContent, fetchAllMetrics }) => {
    const [name, setName] = useState('');
    const [modalIsOpen, setModalIsOpen] = useState(null);
    const [modalSaveOpen, setModalSaveOpen] = useState(null);

    useEffect(() => {
        setName(metricsContent.name);
    }, [metricsContent]);

    const putUpdatedMetric = async (e) => {
        e.preventDefault();
        const updatedMetric = {
            id: metricsContent.id,
            name: name
        };
        setModalSaveOpen(true);
        if (modalSaveOpen) {
            await putRequest(updatedMetric);
        }
    }

    const putRequest = async (updatedMetric) => {
        await fetch(`http://localhost:8080/api/metric/update/`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(updatedMetric)
        });
        fetchAllMetrics(); // Refreshes the list and loads the buttons
    }

    const deleteMetric = async () => {
        await fetch(`http://localhost:8080/api/metric/delete/${metricsContent.id}`, {
            method: 'DELETE'
        });
        fetchAllMetrics();
    }

    const closeModalHandler = () => {
        setModalIsOpen(false);
        setModalSaveOpen(false);
    }

    const openModalHandler = () => {
        setModalIsOpen(true);
    }

    return (
        <div className="div-content">
            <form className="metricContent-form" onSubmit={putUpdatedMetric}>
                <label>Metric naam: </label>
                <textarea value={name} onChange={e => setName(e.target.value)} />
                <div className="div-save-button">
                    <button className="button-delete" onClick={openModalHandler}>Verwijder metric</button>
                    <input className="button-save" type="submit" value="Opslaan" />
                </div>
            </form>
            {(modalIsOpen || modalSaveOpen) && <Backdrop cancelModal={closeModalHandler} />}
            {checkModal(modalIsOpen, modalSaveOpen, closeModalHandler, deleteMetric, putUpdatedMetric)}
        </div>
    );
}

export default MetricsContent;