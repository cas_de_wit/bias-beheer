import { useState, useEffect } from "react";
import Backdrop from "../modal/Backdrop";
import checkModal from "../../Functions/checkModal";

const NewsArticleContent = ({ newsArticleContent, fetchAllNewsArticles }) => {
    const [title, setTitle] = useState('');
    const [message, setMessage] = useState('');
    const [source, setSource] = useState('');
    const [popUp, setPopUp] = useState(false);
    const [modalIsOpen, setModalIsOpen] = useState(null);
    const [modalSaveOpen, setModalSaveOpen] = useState(null);

    useEffect(() => {
        setTitle(newsArticleContent.title);
        setMessage(newsArticleContent.message);
        setSource(newsArticleContent.source);
        setPopUp(newsArticleContent.popUp);
    }, [newsArticleContent]);

    const putUpdatedNewsArticle = async (e) => {
        e.preventDefault();
        const updatedNewsArticle = {
            id: newsArticleContent.id,
            title: title,
            message: message,
            source: source,
            popUp: popUp
        };
        setModalSaveOpen(true);
        if (modalSaveOpen) {
            await putRequest(updatedNewsArticle);
        }
    }

    const putRequest = async (updatedNewsArticle) => {
        await fetch(`http://localhost:8080/api/news_article/update/`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(updatedNewsArticle)
        })
        fetchAllNewsArticles();
    }

    const deleteNewsArticle = async () => {
        await fetch(`http://localhost:8080/api/news_article/delete/${newsArticleContent.id}`, {
            method: 'DELETE'
        });
        fetchAllNewsArticles();
    }

    const closeModalHandler = () => {
        setModalIsOpen(false);
        setModalSaveOpen(false);
    }

    const openModalHandler = () => {
        setModalIsOpen(true);
    }

    return (
        <div className="div-content">
            <form className="newsArticleContent-form" onSubmit={putUpdatedNewsArticle}>
                <label>Nieuwsartikel titel</label>
                <textarea value={title} onChange={e => setTitle(e.target.value)}/>
                <label>Nieuwsartikel inhoud</label>
                <textarea value={message} onChange={e => setMessage(e.target.value)}/>
                <label>Nieuwsartikel bron</label>
                <textarea value={source} onChange={e => setSource(e.target.value)}/>
                <label>Nieuwsartikel pop-up</label>
                <input type="checkbox" value={popUp} className="checkbox-popup" onChange={e =>
                    setPopUp(e.target.checked)} checked={popUp} />
                <div className="div-save-button">
                    <button className="button-delete" onClick={openModalHandler}>Verwijder nieuwsartikel</button>
                    <input className="button-save" type="submit" value="Opslaan"/>
                </div>
            </form>
            {(modalIsOpen || modalSaveOpen) && <Backdrop cancelModal={closeModalHandler} />}
            {checkModal(modalIsOpen, modalSaveOpen, closeModalHandler, deleteNewsArticle, putUpdatedNewsArticle)}
        </div>
    );
}

export default NewsArticleContent;
